module.exports = {
  name: 'angular-rust-foo',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/angular-rust-foo',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
