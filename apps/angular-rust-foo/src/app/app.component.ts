import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message } from '@angular-rust-foo/api-interfaces';

@Component({
  selector: 'angular-rust-foo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  hello$ = this.http.get<Message>('/api/hello');

  constructor(private http: HttpClient) {
    import('wasmwasm')
      .then(w => w.greet('Lukas'));
  }
}
