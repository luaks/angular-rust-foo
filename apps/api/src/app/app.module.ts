import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

const CLIENT_PATH: string = process.env.NODE_ENV === 'prod' && process.env.CLIENT_PATH
  ? process.env.CLIENT_PATH
  : join(__dirname, '..', 'angular-rust-foo');

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: CLIENT_PATH
    })
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {
}
