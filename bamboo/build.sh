#!/usr/bin/env bash

set -e

pushd $(dirname $0)/..

docker run -t --rm -v $(dirname $0)/wasmwasm:/app -w /app --net=host nrayamajhee/arch-wasm-pack wasm-pack build

npm ci
npm run nx -- affected:test
npm run nx -- affected:build

docker build \
  --network=host \
  -t angular-rust-test:$BAMBOO_BUILD_ID \
  --iidfile ./image.metadata \
  -f ./bamboo/be.dockerfile \
  ./
