FROM node:10-alpine

COPY ./dist/apps/api /app
COPY ./dist/apps/angular-rust-foo /app/client
COPY ./package.json /app/package.json
COPY ./package-lock.json /app/package-lock.json
ENV NODE_ENV=production
ENV CLIENT_PATH=/app/client

WORKDIR /app

RUN npm install --only=production --verbose

CMD ["node", "/app/main.js"]
